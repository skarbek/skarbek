# Hi there, I'm John, just call me `skarbek`

## About

I am from the United States of America and have been in Information Technology in some format since school.  Currently I am a [Staff Engineer] on the [Delivery Team] as a [Site Reliability Engineer].

My username here is `@skarbek`.

### Current Work

1. [Delivery Side of Cell Deployments/Rollbacks](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1235)
1. [Speed up Deployments for Cells](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20067)
1. [Rollback discovery/implementation](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20111)

### Brain :thunder_cloud_rain:

1. [Reworking prior proposal for cells](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/19449)

---

## Journal

* [Journal](https://gitlab.com/skarbek/goals/-/wikis/Journal)
* [Goals](https://gitlab.com/skarbek/goals/-/wikis/Goals)
* [MindMap](https://gitlab.com/skarbek/goals/-/wikis/home#mindmap)

[Staff Engineer]: https://handbook.gitlab.com/handbook/engineering/development/dev/training/staff-engineers/
[Delivery Team]: https://handbook.gitlab.com/handbook/engineering/infrastructure/team/delivery/
[Site Reliability Engineer]: https://handbook.gitlab.com/job-families/engineering/infrastructure/site-reliability-engineer/
